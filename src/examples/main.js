import Vue from 'vue'
import App from './app.vue'

let app = new Vue({
    el: '#app',
    // store,
    // router,
    // methods,
    render: h => h(App)
})