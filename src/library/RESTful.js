import Vue from 'vue'
import { deepCopy, isObject, isArray } from '../plugins/helper.js'

export default function(options,model){
    
    let store = options.store.toUpperCase()
    let MODEL = model.toUpperCase()
    let dgx = options.state[model].options
    if(dgx.url){
        for(let method of ['GET','POST','PUT','DELETE']){
            options.actions[`${method}_${MODEL}`] = ({ state, dispatch, commit },data={})=> {
                // 应用身份
                // 发起请求
                return dispatch(`${store}_AJAX`,{
                    method,
                    url     : data.id ? `${dgx.url}/${data.id}` : dgx.url,
                    data    : data.data || {},
                    params  : data.params,
                    model   : model,
                    only    : data.only !== undefined ? data.only : ( method === 'GET'),
                    silence : data.silence ? true : false
                }).then( res=> {
                    // 不同方法对 store 的影响
                    if(res.data){
                        switch(method){
                            case 'GET':
                                // 如果有 id 
                                if(data.id || res.data.id || res.data.sn){
                                    // commit(`${store}_UPDATE`,{base:model,key:'list',value:[res.data]})
                                    commit(`${store}_UPDATE`,{base:model,key:'item',value:res.data})
                                    commit(`${store}_UPDATE`,{base:model,key:'id',value:data.id})
                                    // commit(`${store}_UPDATE`,{base:model,key:'active',value:0})
                                }else if(Array.isArray(res.data)){
                                    commit(`${store}_UPDATE`,{base:model,key:'list',value:res.data || res})
                                    commit(`${store}_UPDATE`,{base:model,key:'page',value:res.page})
                                    commit(`${store}_UPDATE`,{base:model,key:'total',value:res.total})
                                    commit(`${store}_UPDATE`,{base:model,key:'empty',value:res.page==1 && !res.data.length ? true : false})
                                }else{
                                    commit(`${store}_UPDATE`,{base:model,key:'list',value:res.data || res})
                                    commit(`${store}_UPDATE`,{base:model,key:'item',value:res.data || res})
                                }
                                break;
                            case 'POST':
                                if(!dgx.socket){
                                    commit(`${store}_ADD`,{
                                        base  : model,
                                        key   : 'list',
                                        value : res.data,
                                        position : (data.position || data.position ===0) ? data.position : -1
                                    })
                                }
                                break;
                            case 'PUT':
                                if(!dgx.socket){
                                    commit(`${store}_UPDATE`,{
                                        id    : data.id,
                                        base  : model,
                                        key   : 'list',
                                        value : res.data,
                                        extend : true,
                                    })
                                }
                                break;
                            case 'DELETE':
                                if(!dgx.socket){
                                    commit(`${store}_REMOVE`,{
                                        id    : data.id,
                                        base  : model,
                                        key   : 'list',
                                    }) 
                                }
                                break;
                            default:
                                break;
                        }
                    }
                    if(data.active || data.active == 0){
                        dispatch(`ACTIVE_${MODEL}`,data.active)
                    }
                    return res
                })
            }
        }
    }
}
